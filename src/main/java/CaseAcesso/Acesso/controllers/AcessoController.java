package CaseAcesso.Acesso.controllers;

import CaseAcesso.Acesso.models.Acesso;
import CaseAcesso.Acesso.models.dtos.AcessoMapper;
import CaseAcesso.Acesso.models.dtos.BuscarAcessoResponse;
import CaseAcesso.Acesso.models.dtos.SalvarAcessoRequest;
import CaseAcesso.Acesso.models.dtos.SalvarAcessoResponse;
import CaseAcesso.Acesso.services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @Autowired
    private AcessoMapper acessoMapper;

    @PostMapping("/acesso")
    @ResponseStatus(HttpStatus.CREATED)
    public SalvarAcessoResponse salvarAcesso(@Valid @RequestBody SalvarAcessoRequest salvarAcessoRequest) {
        Acesso acesso = acessoMapper.toAcesso(salvarAcessoRequest);

        return acessoMapper.toSalvarAcessoResponse(acessoService.salvarAcesso(acesso));
    }

    @GetMapping("acesso/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.OK)
    public BuscarAcessoResponse buscarPorClienteEPorta(@PathVariable long cliente_id, @PathVariable long porta_id) {
        Acesso acesso = acessoService.buscarPorClienteIdEPortaId(cliente_id,porta_id);
        return acessoMapper.toBuscarAcessoResponse(acesso);
    }

    @DeleteMapping("acesso/{cliente_id}/{porta_id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarAcesso(@PathVariable long cliente_id, @PathVariable long porta_id) {
        acessoService.deletarAcesso(cliente_id, porta_id);
    }

}
