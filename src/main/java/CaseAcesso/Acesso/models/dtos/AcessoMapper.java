package CaseAcesso.Acesso.models.dtos;

import CaseAcesso.Acesso.models.Acesso;
import org.springframework.stereotype.Component;

@Component
public class AcessoMapper {

    public Acesso toAcesso(SalvarAcessoRequest salvarAcessoRequest) {
        Acesso acesso = new Acesso();
        acesso.setCliente(salvarAcessoRequest.getCliente_id());
        acesso.setPorta(salvarAcessoRequest.getPorta_id());

        return acesso;
    }

    public SalvarAcessoResponse toSalvarAcessoResponse(Acesso acesso) {
        SalvarAcessoResponse salvarAcessoResponse = new SalvarAcessoResponse();
        salvarAcessoResponse.setCliente_id(acesso.getCliente());
        salvarAcessoResponse.setPorta_id(acesso.getPorta());

        return salvarAcessoResponse;

    }

    public BuscarAcessoResponse toBuscarAcessoResponse (Acesso acesso) {
        BuscarAcessoResponse buscarAcessoResponse = new BuscarAcessoResponse();
        buscarAcessoResponse.setCliente_id(acesso.getCliente());
        buscarAcessoResponse.setPorta_id(acesso.getPorta());

        return buscarAcessoResponse;
    }
}
