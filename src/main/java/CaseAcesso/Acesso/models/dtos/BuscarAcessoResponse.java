package CaseAcesso.Acesso.models.dtos;

public class BuscarAcessoResponse {
    private long cliente_id;
    private long porta_id;

    public BuscarAcessoResponse() {
    }

    public BuscarAcessoResponse(long cliente_id, long porta_id) {
        this.cliente_id = cliente_id;
        this.porta_id = porta_id;
    }

    public long getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(long cliente_id) {
        this.cliente_id = cliente_id;
    }

    public long getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(long porta_id) {
        this.porta_id = porta_id;
    }
}
