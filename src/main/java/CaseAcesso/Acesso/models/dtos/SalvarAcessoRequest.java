package CaseAcesso.Acesso.models.dtos;

import javax.validation.constraints.NotNull;

public class SalvarAcessoRequest {

    @NotNull(message = "É obrigatório o preenchimento do cliente")
    private long cliente_id;

    @NotNull(message = "É obrigatório o preenchimento da porta")
    private long porta_id;

    public SalvarAcessoRequest() {
    }

    public SalvarAcessoRequest(@NotNull(message = "É obrigatório o preenchimento do cliente") long cliente_id, @NotNull(message = "É obrigatório o preenchimento da porta") long porta_id) {
        this.cliente_id = cliente_id;
        this.porta_id = porta_id;
    }

    public long getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(long cliente_id) {
        this.cliente_id = cliente_id;
    }

    public long getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(long porta_id) {
        this.porta_id = porta_id;
    }
}
