package CaseAcesso.Acesso.models;

import javax.persistence.*;

@Entity
@Table
public class Acesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long porta;

    private Long cliente;

    public Acesso() {
    }

    public Acesso(Long id, Long porta, Long cliente) {
        this.id = id;
        this.porta = porta;
        this.cliente = cliente;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPorta() {
        return porta;
    }

    public void setPorta(Long porta) {
        this.porta = porta;
    }

    public Long getCliente() {
        return cliente;
    }

    public void setCliente(Long cliente) {
        this.cliente = cliente;
    }
}
