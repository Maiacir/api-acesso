package CaseAcesso.Acesso.clients;

import CaseAcesso.Acesso.models.Cliente;
import CaseAcesso.Acesso.models.Porta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "porta", configuration = PortaClientConfiguration.class)
public interface PortaClient {

    @GetMapping("/porta/{id}")
    Porta buscarPortaPorId(@PathVariable Long id);

}
