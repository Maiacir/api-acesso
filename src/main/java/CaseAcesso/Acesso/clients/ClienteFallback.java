package CaseAcesso.Acesso.clients;

import CaseAcesso.Acesso.exceptions.ServicoClienteOffLineEXception;
import CaseAcesso.Acesso.models.Cliente;
import org.hibernate.annotations.LazyToOne;

import javax.management.ServiceNotFoundException;

public class ClienteFallback implements ClienteClient {

    @Override
    public Cliente buscarClientePorId(Long id) {

        throw new ServicoClienteOffLineEXception();
    }
}
