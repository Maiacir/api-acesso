package CaseAcesso.Acesso.clients;

import CaseAcesso.Acesso.exceptions.PortaNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

import javax.validation.OverridesAttribute;

public class PortaClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new  ErrorDecoder.Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            return new PortaNotFoundException();
        } else {
            return errorDecoder.decode(s, response);
        }
    }
}
