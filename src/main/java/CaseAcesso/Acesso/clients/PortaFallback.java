package CaseAcesso.Acesso.clients;

import CaseAcesso.Acesso.exceptions.ServicoClienteOffLineEXception;
import CaseAcesso.Acesso.exceptions.ServicoPortaOffLineEXception;
import CaseAcesso.Acesso.models.Porta;
import com.google.inject.internal.cglib.proxy.$UndeclaredThrowableException;

public class PortaFallback implements PortaClient {

    @Override
    public Porta buscarPortaPorId(Long id) {
        throw new ServicoPortaOffLineEXception();
    }
}
