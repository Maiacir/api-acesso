package CaseAcesso.Acesso.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Porta não encontrado.")
public class PortaNotFoundException extends RuntimeException {
}
