package CaseAcesso.Acesso.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "Serviço indisponível, aguarde alguns minutos.")
public class ServicoClienteOffLineEXception extends RuntimeException {
}
