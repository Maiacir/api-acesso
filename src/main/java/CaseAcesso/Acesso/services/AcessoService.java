package CaseAcesso.Acesso.services;

import CaseAcesso.Acesso.clients.ClienteClient;
import CaseAcesso.Acesso.clients.PortaClient;
import CaseAcesso.Acesso.exceptions.AcessoNotFoundException;
import CaseAcesso.Acesso.models.Acesso;
import CaseAcesso.Acesso.models.Cliente;
import CaseAcesso.Acesso.models.Porta;
import CaseAcesso.Acesso.repositories.AcessoRepository;
import jdk.nashorn.internal.ir.Optimistic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PortaClient portaClient;

    public Acesso salvarAcesso(Acesso acesso) {
        Cliente cliente = clienteClient.buscarClientePorId(acesso.getCliente());
        Porta porta = portaClient.buscarPortaPorId(acesso.getPorta());

        return acessoRepository.save(acesso);
    }

    public void deletarAcesso(long porta_id, long cliente_id) {
        Optional<Acesso> acessoOptional = acessoRepository.findByPortaAndCliente(porta_id, cliente_id);

        if(!acessoOptional.isPresent()) {
            throw new AcessoNotFoundException();
        }
            acessoRepository.delete(acessoOptional.get());
    }

    public Acesso buscarPorClienteIdEPortaId(long porta_id, long cliente_id) {
        Optional<Acesso> acessoOptional = acessoRepository.findByPortaAndCliente(porta_id, cliente_id);

        if(!acessoOptional.isPresent()) {
            throw new AcessoNotFoundException();
        }
            return acessoOptional.get();
    }

}
