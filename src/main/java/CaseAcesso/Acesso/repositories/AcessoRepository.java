package CaseAcesso.Acesso.repositories;

import CaseAcesso.Acesso.models.Acesso;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, Long> {
    Optional<Acesso> findByPortaAndCliente(long porta, long cliente);
}
